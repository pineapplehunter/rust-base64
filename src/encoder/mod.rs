mod core;
mod encode_string;
mod encode_bytes;

pub use self::encode_string::encode_string;
pub use self::encode_bytes::encode;